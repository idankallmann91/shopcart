const Datastore = require('nedb'),
      path = require('path'),
      obj = {}, db = {};

obj.init = () => {
  db.users = new Datastore({ filename: 'path/to/users', autoload: true });
  db.products = new Datastore({ filename: 'path/to/products', autoload: true });
}

obj.login = (user, cb) => {
  db.users.findOne({email: user.email}, (err, userLog) => {
    if (err || !userLog || user.password !== userLog.password) return cb(false);
    cb(userLog);
  });
}

obj.logout = () => {

}

obj.createUser = (user, cb) => {
  db.users.insert(user, (err, newUser) => {
    if(err) cb(false);
    cb(newUser);
  });
}

obj.getResultProducts = (prod, cb) => {
  db.users.find({sec2: prod}, (err, prods) => {
    console.log(prods);
    if(err) cb(false);
    cb(prods);
  });
}

obj.addNewProduct = (product, cb) => {
  db.products.insert(product, (err, newProd) => {
    if (err) return cb(false);
    cb(newProd);
  })
}

obj.getListProducts = (val,cb) => {
  db.products.find({}, (err, products) => {
    if (err) return cb(false);
    cb(products);
  })
}

obj.getMyProducts = (cb) => {
  db.products.find({}, (err, products) => {
    if (err) return cb(false);
    cb(products);
  })
}



module.exports = obj;
