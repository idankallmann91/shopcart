const { getListProducts } = require('../../db/init.js'),
      // cs = require('./chatSocket.js'),
      groups = [],
      x = { /*chat: cs.build() */ };

module.exports = (server) => {
    const io = require('socket.io')(server),
          connected = {},
          mwSocket = (socket, next) => {
            next();
          };

    io.use(mwSocket);
    io.on('connection', (socket) => {
        connected[socket.id] = socket.id;
        socket.on('search', (val) => {
          getListProducts(val, (prods) => {
            let newArr = prods.filter(prod => {
                let lowercase = prod.sec3.toLowerCase();
                return lowercase.includes(val);
              })
            socket.emit('getFromSearch', newArr);
          })
        });

        socket.on('sendMsg', (val) => {
            socket.emit('getChatMsg', 'test');
        });

        socket.on('disconnect', () => {
            console.log(connected[socket.id]+" was deleted");
            delete connected[socket.id];
        });

    });

    // CHAT SOCKET
    var chatSocket = io.of('/chat');
    var chatUser = [];

    chatSocket.on('connection', (socket) => {
      socket.join('public');
      let query = socket.handshake.query.name
      chatUser[socket.id] = query;

      let updateUsers = () => {
        let userList = [];
        for (var key in chatUser) {
            if (chatUser.hasOwnProperty(key)) {
                userList.push(chatUser[key]);
            }
        }
        chatSocket.in('public').emit('updateUsers', userList);
      };

      chatSocket.in('public').clients((err, clients) => {
        if(err) throw err;
        let newUsers = clients.map(el => chatUser[el]);
        chatSocket.in('public').emit('userConnected', newUsers);
      });

      socket.on('typing', user => {
        socket.broadcast.emit('isTyping', user + ' is typing...');
      });

      socket.on('sendMessage', (obj) => {
        chatSocket.in('public').emit('getMessage', {from: obj.user, messages: obj.message});
      });

      socket.on('disconnect', () => {
          console.log(chatUser[socket.id]+" chat was deleted");
          delete chatUser[socket.id];
          socket.leave('public');
          updateUsers();
      });

    });

}

// const socketBuilder =  (namespace) => {
//
// }
// var x = socketBuilder(x[namepace])
