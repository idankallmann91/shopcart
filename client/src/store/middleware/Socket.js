import io from 'socket.io-client';
var socket, chatSocket;

const middleware = ({dispatch}) => next => action => {
  switch (action.type) {
    // User request to connect socket
    case 'SOCKET:CONNECT':
      socket = io(action.uri);
      socket.on('connect', () => console.log("socket connect"));
      socket.on('error', () => console.log("socket error"));
      socket.on('getFromSearch', res => dispatch({type: 'SOCKET_GET_MESSAGE', data: res}));
      socket.on('getChatMsg', res => dispatch({type: 'CHAT_GET_MSG', data: res}));
      break;

    // User request to send a message
    case 'SOCKET:SEND':
      socket.emit(action.message, action.data);
      break;

    // User request to send a message
    case 'SOCKET:CHAT_CONNECT':
      chatSocket = io(action.uri);
      chatSocket.on('connect', () => {
        let { connected, id } = chatSocket;
        let { name } = action;
        console.log("socket chat connect")
        dispatch({type: 'CHAT_CONNECTION_SUCCESS', connected, id, name});
      });
      chatSocket.on('disconnect', () => dispatch({type: 'CHAT_DISCONNECT'}));
      chatSocket.on('updateUsers', users => dispatch({type: 'CHAT_USER_CONNECTED', data: users}));
      chatSocket.on('userConnected', user => dispatch({type: 'CHAT_USER_CONNECTED', data: user}));
      chatSocket.on('error', err => dispatch({type: 'CHAT_CONNECTION_ERROR'}));
      chatSocket.on('isTyping', res => dispatch({type: 'CHAT_TYPING', data: res}));
      chatSocket.on('getMessage', res => dispatch({type: 'CHAT_GET_MSG', data: res}));
      break;

    case 'SOCKET:CHAT:SEND':
      chatSocket.emit(action.message, action.data);
      break;

    // User request to disconnect
    case 'SOCKET:DISCONNECT':
      socket.close();
      break;

    case 'SOCKET:CHAT:DISCONNECT':
      chatSocket.disconnect();
      break;

    default: // We don't really need the default but ...
      break;
  };
  return next(action);
};

export default middleware;
