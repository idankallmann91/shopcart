import axios from 'axios';

export const ProductSearch = val => {
  return async(dispatch) => {
    const res = await axios.get(`http://localhost:4000/api/v1/search?search=${val}`);
    return new Promise((resolve, reject) => {
      if(res.data.success) {
        dispatch({ type: 'SEARCH_SUCCESS' , data: res.data.data});
        resolve();
      } else {
        dispatch({ type: 'SEARCH_ERROR'});
        reject();
      }
    })
  }
}

export const AddNewProduct = (product) => {
  return async(dispatch) => {
    const { sec1, sec2, sec3, sec2_1, sec2_2, sec2_3, sec2_4, sec2_5 } = product;
    const newObj = {sec1, sec2, sec3, sec2_1, sec2_2, sec2_3, sec2_4, sec2_5 };
    const res = await axios.post('http://localhost:4000/api/v1/addproduct', newObj);
    return new Promise((resolve, reject) => {
      if(res.data.success) {
        dispatch({ type: 'NEWPRODUCT_SUCCESS' , data: res.data});
        resolve();
      } else {
        dispatch({ type: 'NEWPRODUCT_ERROR'});
        reject();
      }
    })
  }
}

export const GetList = (product) => {
  return async(dispatch) => {
    const res = await axios.get('http://localhost:4000/api/v1/getlist');
    return new Promise((resolve, reject) => {
      if(res.data.success) {
        dispatch({ type: 'GETLIST_SUCCESS' , data: res.data.data});
        resolve();
      } else {
        dispatch({ type: 'GETLIST_ERROR'});
        reject();
      }
    })
  }
}

export const GetAllMyProducts = (product) => {
  return async(dispatch) => {
    const res = await axios.get('http://localhost:4000/api/v1/myproducts');
    res.data.success ? dispatch({ type: 'MYPRODUCTS_SUCCESS' ,data: res.data.data})
      : dispatch({ type: 'MYPRODUCTS_ERROR'});
    // return new Promise((resolve, reject) => {
    //   if(res.data.success) {
    //     dispatch({ type: 'MYPRODUCTS_SUCCESS' ,data: res.data.data});
    //     resolve(res);
    //   } else {
    //     dispatch({ type: 'MYPRODUCTS_SUCCESS' ,data: res.data.data});
    //     reject();
    //   }
    // })
  }
}

export default { ProductSearch, AddNewProduct, GetList, GetAllMyProducts };
