const ListForProducts = val => {
  if (val !== '') {
      return {
        type: 'SOCKET:SEND',
        message: 'search',
        payload: val
      }
  } else {
      return { type: 'NOVALUE' };
  }
}

const ConnectChat = userJoin => {
  return (dispatch, getState) => {
      dispatch({
        type: 'SOCKET:CHAT_CONNECT',
        uri: 'http://localhost:4000/chat?name='+userJoin,
        name: userJoin
      });
  }
}

const DiconnectChat = () => {
  return (dispatch, getState) => {
      dispatch({
        type: 'SOCKET:CHAT:DISCONNECT'
      });
  }
}

const SendMessage = obj => {
  return (dispatch) => {
    dispatch({
      type: 'SOCKET:CHAT:SEND',
      message: 'sendMessage',
      data: obj
    });
  }
}

const OnTyping = user => {
  return (dispatch, getState) => {
    dispatch({
      type: 'SOCKET:CHAT:SEND',
      message: 'typing',
      data: user
    });
  }
}
export { ListForProducts , ConnectChat, DiconnectChat, OnTyping, SendMessage };
