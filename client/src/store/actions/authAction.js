import axios from 'axios';

export const SignIn = (credentials) => {
  return (dispatch, getState) => {
    axios.post('http://localhost:4000/api/v1/login', credentials)
      .then(res => {
        res.data.success ? dispatch({ type: 'LOGIN_SUCCESS', user: res.data })
        : dispatch({ type: 'LOGIN_ERROR' });
      })
  }
}

export const SignUp = (user) => {
  return (dispatch, getState) => {
    if (user.email !== user.emailConfirm || user.password !== user.passwordConfirm) {
      dispatch({ type: 'ERROR_FIELDS' });
    } else {
      axios.post('http://localhost:4000/api/v1/signup', user)
        .then(res => {
          res.data.success ? dispatch({ type: 'SIGNUP_SUCCESS' })
          : dispatch({ type: 'SIGNUP_ERROR' });
        })
    }
  }
}

export const SignOut = (credentials) => {
  return (dispatch, getState) => {
    dispatch({ type: 'SIGNOUT' })
  }
}

export const socketTest = () => {
  return (dispatch, getState) => {
    dispatch({ type: 'SIGNOUT' })
  }
}
