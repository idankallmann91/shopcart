const initState = { logged: false, user: null, signup: false, msg: '' };

const authReducer = (state = initState, action) => {
  switch (action.type) {
    case 'LOGIN_SUCCESS':
      state = {
        ...state,
        logged: action.user.success,
        user: action.user.user
      }
      break;
    case 'LOGIN_ERROR':
      state = {
        ...state,
        logged: false,
        user: null,
        msg: 'Invalid email or password'
      }
      break;
    case 'SIGNUP_SUCCESS':
      state = {
        ...state,
        signup: true
      }
      break;
    case 'SIGNUP_ERROR':
      state = {
        ...state,
        signup: false
      }
      break;
    case 'ERROR_FIELDS':
      state = {
        ...state,
        signup: false
      }
      break;
    case 'SIGNOUT':
      state = {
        ...state,
        logged: false,
        user: null
      }
      break;
    default:
    state = {...state}
  }

  return state;
}

export default authReducer;
