const initState = {products: [], show: false, product: '', list: [], myProducts: [], results: []};

const productsReducer = (state = initState, action) => {

  switch (action.type) {
    case 'SOCKET_GET_MESSAGE':
      state = {
        ...state,
        products: action.data,
        product: '',
        show: true,
        list: [],
        myProducts: [],
        results: []
      }
      break;
    case 'NOVALUE':
      state = {
        ...state,
        products: [],
        product: '',
        show: false,
        list: [],
        myProducts: [],
        results: []
      }
      break;
    case 'SEARCH_SUCCESS':
      state = {
        ...state,
        products: [],
        product: action.data,
        show: false,
        list: [],
        myProducts: [],
        results: action.data
      }
      break;
    case 'SEARCH_ERROR':
      state = {
        ...state,
        products: [],
        product: '',
        show: false,
        list: [],
        myProducts: [],
        results: []
      }
      break;
      case 'GETLIST_SUCCESS':
        state = {
          ...state,
          products: [],
          product: action.data.data,
          show: false,
          list: action.data,
          myProducts: [],
          results: []
        }
        break;
      case 'GETLIST_ERROR':
        state = {
          ...state,
          products: [],
          product: '',
          show: false,
          list: [],
          myProducts: [],
          results: []
        }
        break;
      case 'NEWPRODUCT_SUCCESS':
        state = {
          ...state,
          products: [],
          product: '',
          show: false,
          list: [],
          myProducts: [],
          results: []
        }
        break;
      case 'NEWPRODUCT_ERROR':
        state = {
          ...state,
          products: [],
          product: '',
          show: false,
          list: [],
          myProducts: [],
          results: []
        }
        break;
      case 'MYPRODUCTS_SUCCESS':
        state = {
          ...state,
          products: [],
          product: '',
          show: false,
          list: [],
          myProducts: action.data,
          results: []
        }
        break;
      case 'MYPRODUCTS_ERROR':
        state = {
          ...state,
          products: [],
          product: '',
          show: false,
          list: [],
          myProducts: [],
          results: []
        }
        break;
    default:

  }
  return state;
}

export default productsReducer;
