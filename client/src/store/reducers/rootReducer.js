import { combineReducers } from 'redux';

import authReducer from './authReducer';
import productsReducer from './productsReducer';
import chatReducer from './chatReducer';

const rootReducer = combineReducers({ authReducer, productsReducer, chatReducer });

export default rootReducer;
