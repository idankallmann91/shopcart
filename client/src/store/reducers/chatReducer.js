const initState = { msg: '', messages: [], user:'',
                    users:[], connected: false,
                    typing: false, from: '', color: ''
                  };

const chatReducer = (state = initState, action) => {
  switch (action.type) {
    case 'CHAT_CONNECTION_SUCCESS':
      state = {
        ...state,
        messages: [],
        connected: action.connected,
        user: action.name
      }
      break;

    case 'CHAT_CONNECTION_ERROR':
      state = {
        ...state,
        connected: false
      }
      break;

    case 'CHAT_USER_CONNECTED':
      state = {
        ...state,
        users: [...action.data]
      }
      break;

    case 'CHAT_TYPING':
      console.log(action);
      state = {
        ...state,
        typing: true,
        msg: action.data
      }
      break;

    case 'CHAT_GET_MSG':
      state = {
        ...state,
        messages: [...state.messages, action.data],
        typing: false,
        from: action.data.from
      }
      break;

    case 'CHAT_DISCONNECT':
      state = {
        ...state,
        connected: false,
        users: [...state.users.splice(state.user, 1)]
      }
      break;

    default:
    state = {...state}
  }

  return state;
}

export default chatReducer;
