import React, { Component } from 'react';
import { Redirect, Route , NavLink, Switch  } from 'react-router-dom';
import { connect } from 'react-redux';

import './Dashboard.css';
import { Collapsible, CollapsibleItem, SideNav, SideNavItem  } from 'react-materialize';

import { GetList, AddNewProduct, GetAllMyProducts } from '../../store/actions/requestsAction'

import Img from '../../img.jpg';

//CHILD COMPONENTS
import AddProducts from '../layouts/AddProducts';
import Products from '../layouts/Products';
import MyProfile from '../layouts/MyProfile';
import Setting from '../layouts/Setting';

class Dashboard extends Component {

  constructor(props) {
    super(props);

    this.state = {
      type: 'ADD PRODUCT',
      products: [],
      imagePreview: '',
      windowWidth: window.innerWidth
    }
  }

  componentDidMount() {
      window.addEventListener("resize", this.handleWidth);
  }
  componentWillUnmount() {
      window.removeEventListener("resize", this.handleWidth);
  }

  handleWidth = () => this.setState({windowWidth: window.innerWidth});
  // handleStepper = e => {
  //   e.preventDefault();
  // }

  // handleOnChange = e => {
  //   e.persist();
  //   this.setState(prevState => ({
  //       user: {
  //           ...prevState.user,
  //           [e.target.id]: e.target.value
  //       }
  //   }))
  // }

  // handleOnChangeFile = e => {
  //   let userFile = e.target.files[0];
  //   if (!userFile) {
  //     return this.setState(prevState => ({
  //         user: {...prevState.user, file: null},
  //         imagePreview: ''
  //     }))
  //   }
  //   let reader = new FileReader();
  //   reader.readAsDataURL(userFile);
  //   reader.onloadend = () => {
  //     this.setState(prevState => ({
  //         user: {...prevState.user, file: userFile},
  //         imagePreview: reader.result
  //     }))
  //   }
  // }

  render() {
    const { windowWidth } = this.state
    const { auth, user } = this.props;
    // const { imagePreview } = this.state;
    // console.log(this.props);
    if(auth) return <Redirect to="/login" />
    // const imgPrev = imagePreview ?
    // <img id="previewImage" className="circle responsive-img z-depth-3" src={imagePreview} />
    // : <i className="fas fa-user"></i>
    return (
      <div className="dashboard container">
          <div className="dashboard_left">
              { windowWidth < 768 ? <SideNav
                  trigger={<button type="button" className="btn waves-effect waves-light">SIDE NAV DEMO</button>}
                  options={{ closeOnClick: true }}
                  >
                  <SideNavItem userView
                    user={{
                      background: 'bg-toys.jpeg',
                      image: Img,
                      name: 'John Doe',
                      email: 'jdandturk@gmail.com'
                    }}
                  />
                  <SideNavItem href='#!icon' icon='cloud'>First Link With Icon</SideNavItem>
                  <SideNavItem href='#!second'>Second Link</SideNavItem>
                  <SideNavItem divider />
                  <SideNavItem subheader>Subheader</SideNavItem>
                  <SideNavItem waves href='#!third'>Third Link With Waves</SideNavItem>
              </SideNav> : <div>
              <h5>HI {'test' ||user.first_name}!</h5>
              <hr />
              <Collapsible accordion>
                  <CollapsibleItem header='My Profile' icon='person'>
                      <NavLink to="/dashboard/profile" id="profile">profile</NavLink>
                  </CollapsibleItem>
                  <CollapsibleItem header='My Prodocuts' icon='storage'>
                      <div>
                          <NavLink to="/dashboard/products" id="products">products</NavLink>
                      </div>
                      <div>
                          <NavLink to="/dashboard/addproducts" id="addproduct">add products</NavLink>
                      </div>
                  </CollapsibleItem>
                  <CollapsibleItem header='Settings' id="settings" icon='settings_applications'>
                      <NavLink to="/dashboard/settings">settings</NavLink>
                  </CollapsibleItem>
              </Collapsible></div> }
          </div>
          <div className="dashboard_right">
              <h5>{this.state.type}</h5>
              <hr />
              <Switch>
                  <Route path="/dashboard/addproducts" render={() => <AddProducts {...this.props} />}></Route>
                  <Route path="/dashboard/products" render={() => <Products {...this.props} />}></Route>
                  <Route path='/dashboard/profile' render={() => <MyProfile />}></Route>
                  <Route path="/dashboard/settings" component={Setting}></Route>
              </Switch>
          </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.authReducer.logged,
    user: state.authReducer.user,
    list: state.productsReducer.list,
    myProducts: state.productsReducer.myProducts
  }
}

const mapDispathToProps = (dispatch) => {
  return {
    getList: () => dispatch(GetList()),
    addNewProduct: obj => dispatch(AddNewProduct(obj)),
    getAllMyProducts: () => dispatch(GetAllMyProducts())
  }
}

export default connect(mapStateToProps, mapDispathToProps)(Dashboard);
