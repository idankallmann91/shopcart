import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import './Results.css';

import { connect } from 'react-redux';

// import { listForProducts } from '../../../store/actions/socketAction';

import Filters from './filters/Filters';

class Results extends Component {
  constructor(props) {
    super(props);

    this.state = {
      redirect: false,
      state: ''
    };
  }

  componentDidMount() {
    // const { state, search } = this.props.location;
    // const { action } = this.props.history;
    // console.log(state);
    // if (search.lneght <= 7 || (state === undefined || state === "")) {
    //   this.setState({redirect: true});
    // }
    // else if (action === 'REPLACE') this.setState({state: state});
    // else if (action === 'POP' &&  search !== "") {
    //   let val = search.split("=")[1];
    //   SocketService.searching(val, res => {
    //     if(res.length === 0) return this.setState({redirect: true});
    //     this.setState({state: res[0].item});
    //   });
    // }
  }

  render() {
    const { data, state } = this.props.location;
    if (state === undefined) return <Redirect to={{pathname:"/"}} />
    return (
      <main className="results">
          <div className="container">
              <section className="results_header">
                  <article>
                      <h3>Results For {state}</h3>
                      <Filters />
                  </article>
              </section>
              <section className="results_body">
                  <article>
                      <div>
                          <ul>
                              <li>Link1</li>
                              <li>Link2</li>
                              <li>Link3</li>
                              <li>Link4</li>
                              <li>Link5</li>
                              <li>Link6</li>
                              <li>Link7</li>
                              <li>Link8</li>
                          </ul>
                      </div>
                  </article>
              </section>
          </div>
      </main>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    redirect: state.productsReducer.redirect
  }
}

export default connect(mapStateToProps, null)(Results);
