import React from 'react';
import './Filters.css';

const Filters = ({filterOnChange}) => {
  return (
      <div className="filters grey lighten-3 z-depth-2">
          <div className="filters_header">
              <h5>Filters</h5>
              <i className="fas fa-filter"></i>
          </div>
          <div className="filters_body">
              <div>
                  <div>
                      <input type="checkbox" name="group1" id="1" value="green" />
                      <label htmlFor="1">green</label>
                  </div>
                  <div>
                      <input type="checkbox" name="group1" id="2" value="blue" />
                      <label htmlFor="2">blue</label>
                  </div>
                  <div>
                      <input type="checkbox" name="group1" id="3" value="yellow" />
                      <label htmlFor="3">yellow</label>
                  </div>
                  <div>
                      <input type="checkbox" name="group1" id="4" value="red" />
                      <label htmlFor="4">red</label>
                  </div>
              </div>
              <div>
                  <div>
                      <input type="checkbox" name="group2" id="5" value="value" />
                      <label htmlFor="5">Text</label>
                  </div>
                  <div>
                      <input type="checkbox" name="group2" id="6" value="value" />
                      <label htmlFor="6">Text</label>
                  </div>
                  <div>
                      <input type="checkbox" name="group2" id="7" value="value" />
                      <label htmlFor="7">Text</label>
                  </div>
                  <div>
                      <input type="checkbox" name="group2" id="8" value="value" />
                      <label htmlFor="8">Text</label>
                  </div>
              </div>
              <div>
                  <div>
                      <input type="checkbox" name="group3" id="9" value="value" />
                      <label htmlFor="9">Text</label>
                  </div>
                  <div>
                      <input type="checkbox" name="group3" id="10" value="value" />
                      <label htmlFor="10">Text</label>
                  </div>
                  <div>
                      <input type="checkbox" name="group3" id="11" value="value" />
                      <label htmlFor="11">Text</label>
                  </div>
                  <div>
                      <input type="checkbox" name="group3" id="12" value="value" />
                      <label htmlFor="12">Text</label>
                  </div>
              </div>
          </div>
      </div>
  );
}

export default Filters;
