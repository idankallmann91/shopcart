import React, { Component } from 'react';
import { Link, NavLink } from "react-router-dom";
import { connect } from 'react-redux'

import { Modal, Dropdown } from 'react-materialize'
import './Header.css';

//CHILD COMPONENT
import Signup from '../layouts/Signup';

//REDUX AUTH ACTION
import { SignOut } from '../../store/actions/authAction';

class Header extends Component {

   logout = () => this.props.logOut();

   render() {
     const { user, auth } = this.props;
     const userName = auth ? user.first_name.charAt(0) + user.last_name.charAt(0) : <i className="material-icons">person</i> ;
     return (
       <nav className="teal accent-4">
           <div className="nav-wrapper container">
               <Link to="/" className="brand-logo left">LOGO</Link>
               <ul id="nav-mobile" className="right">
                   <li><NavLink to="/">Home</NavLink></li>
                   <li><NavLink to="/chat">Chat</NavLink></li>
                   <li><NavLink to="/dashboard">Dashboard</NavLink></li>
                   <Dropdown
                       trigger={<li id="dropdown"><NavLink to="#" className="dropdown-trigger btn btn-floating teal lighten-3 z-depth-3">{userName}</NavLink></li>}>
                       { !auth ? <li><NavLink to="/login">Login</NavLink></li> : null}
                       { !auth ? <Modal
                           id="modal"
                           header='Sign Up'
                           trigger={<li><NavLink to="#">Signup</NavLink></li>}>
                           <Signup />
                       </Modal> : null}
                       <li className="divider" tabIndex="-1"></li>
                       { auth ? <li><NavLink to="/" onClick={this.logout}>Logout</NavLink></li> : null}
                   </Dropdown>
                   <li><NavLink to="/"><i className="material-icons">shopping_cart</i></NavLink></li>
               </ul>
           </div>
       </nav>
     );
   }

  }

  const mapStateToProps = (state) => {
    return {
      auth: state.authReducer.logged,
      user: state.authReducer.user
    }
  }

  const mapDispathToProps = (dispatch) => {
    return {
      logOut: () => dispatch(SignOut())
    }
  }


export default connect(mapStateToProps, mapDispathToProps)(Header);
