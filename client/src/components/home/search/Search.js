import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import './Search.css';

class Search extends Component {

  constructor(props) {
    super(props);

    this.state = {
      value: '',
      redirect: false
    }
  }

  onSearchChange = e => {
    let val = e.target.value;
    this.setState({value: val});
    this.props.listProducts(val);
  }

  searchItem = val => {
    this.props.searchProduct(val).then(() => {
      this.setState({redirect: true});
    }).catch((ex) => console.log(ex))
  }

  render() {
    const { value, redirect } = this.state;
    const { products, product, show, results } = this.props;
    const productsList = products ? products.map((el,i) => {
      console.log(el);
      return <li key={el._id} className="search_list_item" onMouseDown={() => this.searchItem(el.sec3)}>{el.sec3}</li>
    }) : null;
    console.log(this.props);
    if (redirect) return <Redirect to={{pathname: '/result', search: 'search='+product, data: results , state: product}} />;
    return (
      <div className="search">
          <div className="col s12">
              <div className="row">
                  <div className="input-field col s12">
                      <i className="material-icons prefix">search</i>
                      <input type="text"
                          id="inputSearch"
                          className="autocomplete"
                          onChange={this.onSearchChange}
                          value={value}
                       />
                       { (show && products.length > 0) ?
                         <ul className="search_list white z-depth-4">
                            { productsList }
                         </ul> : null
                       }
                      <label htmlFor="inputSearch">Search</label>
                  </div>
              </div>
          </div>
      </div>
    );
  }
}

export default Search;
