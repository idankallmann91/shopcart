import React from 'react';
import Img from '../../../img.jpg';

const Card = () => {
  return (
    <div className="col s12 m4 l3">
        <div className="card z-depth-4">
            <div className="card-image">
                <img src={Img} alt=""/>
                <span className="card-title">Card Title</span>
                <button className="btn-floating halfway-fab waves-effect waves-light red"><i className="material-icons">add</i></button>
            </div>
            <div className="card-content">
                <p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
            </div>
        </div>
    </div>
  );
}

export default Card;
