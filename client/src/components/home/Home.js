import React, { Component } from 'react';
import './Home.css';

//REDUX
import { connect } from 'react-redux';
import { ProductSearch } from '../../store/actions/requestsAction';
import { ListForProducts } from '../../store/actions/socketAction';

// Child Components
import Search from './search/Search';

class Home extends Component {

  componentDidMount(prop) {
    if(this.props.location.pathname !== '/') window.location.pathname = '/';
  }

  render() {
    return (
      <main className="main">
          <section className="main_top">
              <div className="container">
                  <Search {...this.props} />
              </div>
          </section>
          <section className="main_top_bottom">
              <div className="container">
              </div>
          </section>
          <section className="main_body row">
              <div className="container">

              </div>
          </section>
          <section className="main_footer">

          </section>
      </main>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    products: state.productsReducer.products,
    product: state.productsReducer.product,
    show: state.productsReducer.show,
    results: state.productsReducer.results
  }
}

const mapDispathToProps = (dispatch) => {
  return {
    listProducts: val => dispatch(ListForProducts(val)),
    searchProduct: val => dispatch(ProductSearch(val))
  }
}

export default connect(mapStateToProps, mapDispathToProps)(Home);
