import React, { Component } from 'react';
import './Login.css';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

// REDUX AUTH ACTION
import { SignIn } from '../../store/actions/authAction';

class Login extends Component {
  state = {
    email: '',
    password: '',
  }

  handleChange = e => this.setState({[e.target.type]: e.target.value});

  handleSubmit = e => {
    e.preventDefault();
    this.props.signIn(this.state);
  }

  render() {
    const { auth, msg } = this.props;
    if(auth) return <Redirect to="/dashboard" />
    const errorMessage = msg ? <span className="helper-text">{msg}</span> : null;
    return (
      <div className="login container z-depth-4">
          <form onSubmit={this.handleSubmit} className="col s12" autoComplete="off">
              <div className="input-field col s6">
                  <i className="material-icons prefix" style={msg ? {color: '#F44336'} : {} }>account_circle</i>
                  <input id="icon_prefix" type="email" onChange={this.handleChange} className={ !msg ? 'validate' : 'validate invalid'} />
                  <label htmlFor="icon_prefix">Email</label>
                  {errorMessage}
              </div>
              <div className="input-field col s6">
                  <i className="material-icons prefix" style={msg ? {color: '#F44336'} : {} }>security</i>
                  <input id="icon_telephone" type="password" onChange={this.handleChange} className={ !msg ? 'validate' : 'validate invalid'} />
                  <label htmlFor="icon_telephone">Password</label>
                  {errorMessage}
              </div>
              <button className="btn waves-effect waves-light" type="submit" name="action">Submit
                  <i className="material-icons right">send</i>
              </button>
          </form>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.authReducer.logged,
    msg: state.authReducer.msg
  }
}

const mapDispathToProps = (dispatch) => {
  return {
    signIn: (creds) => dispatch(SignIn(creds))
  }
}

export default connect(mapStateToProps, mapDispathToProps)(Login);
