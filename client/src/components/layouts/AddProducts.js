import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import { Input, Chip } from 'react-materialize';

class AddProduct extends Component {
  constructor(props) {
    super(props);

    this.state = {
      sec1: '', sec2: '', sec3: '', sec2_1: '',
      sec2_2: '', sec2_3: '', sec2_4: '', sec2_5: '',
      id: 0, id1: 0, btn1: false, btn2: false,
      btnBack: false, listD: [], redirect: false
    }
  }

  componentDidMount() {
    this.props.getList().then(() => {
      this.setState({listD: this.props.list});
    }).catch(ex => {
      console.log(ex);
    })
  }

  change1 = e => {
    let id;
    [...e.target.childNodes].map(el => {
      if(el.value === e.target.value && el.value !== '') {
        id = el.id;
      }
      return id;
    })
    this.setState({sec1: e.target.value, sec2: '', sec3: '', id: id, sec2_1: '', sec2_2: '', sec2_3: '', sec2_4: '', sec2_5: ''});
  }

  change2 = e => {
    let id;
    [...e.target.childNodes].map(el => {
      if(el.value === e.target.value && el.value !== '') {
        id = el.id;
      }
      return id;
    })
    this.setState({sec2: e.target.value, sec3: '', id1: id, sec2_1: '', sec2_2: '', sec2_3: '', sec2_4: '', sec2_5: ''});
  }

  change3 = e => this.setState({sec3: e.target.value, btnBack: false, sec2_1: '', sec2_2: '', sec2_3: '', sec2_4: '', sec2_5: ''});
  handleClick = () => this.setState({btn1: true, btnBack: false});
  handleClickNext = () => this.setState({});
  handleClickBack = () => this.setState({btnBack: true, btn1: false});
  change2_1 = e => {
    this.setState({[e.target.childNodes[0].className]: e.target.value});
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.addNewProduct(this.state).then(() => this.setState({redirect: true}));
  }

  render() {
    const { sec1, sec2, sec3, btn1, btnBack, listD, id, id1, sec2_1, sec2_2, sec2_3, sec2_4, sec2_5, redirect } = this.state;
    if(redirect) return <Redirect to="/dashboard/products" />;
    const list1 = listD.map((el, i) => <option value={el.category} id={i} key={i}>{el.category}</option>);
    const list2 = (listD.length > 0 && sec1) ? listD[id].subCategory.map((el, i) => {
          let key = Object.keys(el);
          return <option value={key} id={i} key={i}>{key}</option>;
      }) : null;
      const list3 = (listD.length > 0 && sec1 && sec2) ? listD[id].subCategory[id1][sec2].map((el, i) => {
          return <option value={el} key={i}>{el}</option>;
        }) : null;
    return (
      <div className="newproduct">
          <div className="row">
              <form onSubmit={this.handleSubmit} className="col s12" autoComplete="off">
                  { sec1 || sec2 || sec3 ? <div className="row">
                      <Chip>{sec1}</Chip>
                      { sec2 ? <Chip>{sec2}</Chip> : null }
                      { sec3 ? <Chip>{sec3}</Chip> : null }
                      { sec2_1 ? <Chip>{sec2_1}</Chip> : null }
                      { sec2_2 ? <Chip>{sec2_2}</Chip> : null }
                      { sec2_3 ? <Chip>{sec2_3}</Chip> : null }
                      { sec2_4 ? <Chip>{sec2_4}</Chip> : null }
                      { sec2_5 ? <Chip>{sec2_5}</Chip> : null }
                  </div> : null}
                  <hr />
                  { btn1 ? null : <div>
                    <div className="row">
                        <Input s={11} type='select' name="idan" onChange={this.change1} label="Choose Category" defaultValue={sec1}>
                            <option value=""></option>
                            {list1}
                        </Input>
                    </div>
                    <div className="row">
                        { sec1 ? <Input s={11} type='select' onChange={this.change2} label={sec1} defaultValue={sec2}>
                            <option value=""></option>
                            {list2}
                        </Input> : null }
                    </div>
                    <div className="row">
                        { sec2 && sec1 ? <Input s={11} type='select' onChange={this.change3} label={sec2} defaultValue={sec3}>
                            <option value=""></option>
                            {list3}
                        </Input> : null }
                    </div>
                     <div className="row">
                         <button type="button" onClick={this.handleClick} disabled={!sec3} className="waves-effect waves-light btn right">Next</button>
                     </div>
                  </div> }
                  { btn1 ? <div>
                      <div className="row">
                          <Input s={4} type="select" id="sec2_1" onChange={this.change2_1} label="General" validate defaultValue={sec2_1}>
                              <option className="sec2_1" value=""></option>
                              <option className="sec2_1" value="option1">option2_1</option>
                              <option className="sec2_1" value="option2">option2_2</option>
                              <option className="sec2_1" value="option3">option2_3</option>
                              <option className="sec2_1" value="option4">option2_4</option>
                              <option className="sec2_1" value="option5">option2_5</option>
                          </Input>
                          <Input s={4} type="select" id="sec2_2" onChange={this.change2_1} label="Key Features" validate defaultValue={sec2_2}>
                              <option className="sec2_2" value=""></option>
                              <option className="sec2_2" value="option1">option2_1</option>
                              <option className="sec2_2" value="option2">option2_2</option>
                              <option className="sec2_2" value="option3">option2_3</option>
                              <option className="sec2_2" value="option4">option2_4</option>
                              <option className="sec2_2" value="option5">option2_5</option>
                          </Input>
                          <Input s={4} type="select" id="sec2_3" onChange={this.change2_1} label="Technical Data" validate defaultValue={sec2_3}>
                              <option className="sec2_3" value=""></option>
                              <option className="sec2_3" value="option1">option2_1</option>
                              <option className="sec2_3" value="option2">option2_2</option>
                              <option className="sec2_3" value="option3">option2_3</option>
                              <option className="sec2_3" value="option4">option2_4</option>
                              <option className="sec2_3" value="option5">option2_5</option>
                          </Input>
                          <Input s={6} type="select" id="sec2_4" onChange={this.change2_1} label="Display Properties" validate defaultValue={sec2_4}>
                              <option className="sec2_4" value=""></option>
                              <option className="sec2_4" value="option1">option2_1</option>
                              <option className="sec2_4" value="option2">option2_2</option>
                              <option className="sec2_4" value="option3">option2_3</option>
                              <option className="sec2_4" value="option4">option2_4</option>
                              <option className="sec2_4" value="option5">option2_5</option>
                          </Input>
                          <Input s={6} type="select" id="sec2_5" onChange={this.change2_1} label="Connections And Connectivity" validate defaultValue={sec2_5}>
                              <option className="sec2_5" value=""></option>
                              <option className="sec2_5" value="option1">option2_1</option>
                              <option className="sec2_5" value="option2">option2_2</option>
                              <option className="sec2_5" value="option3">option2_3</option>
                              <option className="sec2_5" value="option4">option2_4</option>
                              <option className="sec2_5" value="option5">option2_5</option>
                          </Input>
                      </div>
                      <div className="row">
                          <button type="button" onClick={this.handleClickBack} className="waves-effect waves-light btn left">Back</button>
                          <button type="submit" disabled={!sec2_5} className="waves-effect waves-light btn right">Submit</button>
                      </div>
                  </div> : null }
              </form>
         </div>
      </div>
    );
  }

}

export default AddProduct;
