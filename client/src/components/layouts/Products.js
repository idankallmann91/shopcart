import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Img from '../../img.jpg'
class Products extends Component {
  constructor(props) {
    super(props);

    this.state = {
      products: []
    }
  }

  componentDidMount() {
    this.props.getAllMyProducts().then(() => {
      this.setState({products: this.props.myProducts});
    }).catch(ex => {
      console.log(ex);
    })
  }

  render() {
    const { products } = this.state
    const showProducts = products.map(el => {
      return (
        <div className="col s12 m6 l4" key={el._id}>
            <div className="card small">
                <div className="card-image waves-effect waves-block waves-light">
                    <img className="activator" src={Img} />
                </div>
                <div className="card-content">
                    <span className="card-title activator grey-text text-darken-4">{el.sec3}<i className="material-icons right">more_vert</i></span>
                    <Link to={`/dashboard/products/${el.sec2}`}>Link</Link>
                </div>
                <div className="card-reveal">
                    <span className="card-title grey-text text-darken-4">{el.sec1}<i className="material-icons right">close</i></span>
                    <ul>
                      <li>{el.sec2}</li>
                      <li>{el.sec3}</li>
                      <li>{el.sec2_1}</li>
                      <li>{el.sec2_2}</li>
                      <li>{el.sec2_3}</li>
                      <li>{el.sec2_4}</li>
                      <li>{el.sec2_5}</li>
                    </ul>
                </div>
            </div>
        </div>
      )
    })
    return (
      <div className="myproducts">
          <div className="row">
              {showProducts}
          </div>
      </div>
    );
  }
}

export default Products;
