import React, { Component } from 'react';
// import './Login.css';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

//REDUX AUTH ACTION
import { SignUp } from '../../store/actions/authAction';

class Signup extends Component {

  constructor(props) {
    super(props);

    this.state = {
      first_name: '',
      last_name: '',
      email: '',
      emailConfirm: '',
      password: '',
      passwordConfirm: '',
      redirect: false
    }
  }

  handleChange = e => this.setState({[e.target.id]: e.target.value});

  handleSubmit = e => {
    e.preventDefault();
    this.props.signUp(this.state);
  }

  render() {
    const { auth } = this.props;
    if(auth) return <Redirect to="/login" />
    return (
        <div className="row">
            <form onSubmit={this.handleSubmit} className="s12" autoComplete="off">
                <div className="row">
                  <div className="input-field col m6 s12">
                    <input id="first_name" type="text" onChange={this.handleChange} className="validate" />
                    <label htmlFor="first_name">First Name</label>
                  </div>
                  <div className="input-field col m6 s12">
                    <input id="last_name" type="text" onChange={this.handleChange} className="validate" />
                    <label htmlFor="last_name">Last Name</label>
                  </div>
                </div>
                <div className="row">
                    <div className="input-field col m6 s12">
                      <input id="password" type="password" onChange={this.handleChange} className="validate" />
                      <label htmlFor="password">Password</label>
                    </div>
                    <div className="input-field col m6 s12">
                      <input id="passwordConfirm" type="password" onChange={this.handleChange} className="validate" />
                      <label htmlFor="passwordConfirm">Password Confirm</label>
                    </div>
                </div>
                <div className="row">
                    <div className="input-field col m6 s12">
                      <input id="email" type="email" onChange={this.handleChange} className="validate" />
                      <label htmlFor="email">Email</label>
                    </div>
                    <div className="input-field col m6 s12">
                      <input id="emailConfirm" type="email" onChange={this.handleChange} className="validate" />
                      <label htmlFor="emailConfirm">Email Confirm</label>
                    </div>
                </div>
                <button className="btn waves-effect waves-light modal-close" type="submit" name="action">Submit
                    <i className="material-icons right">send</i>
                </button>
            </form>
        </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.authReducer.signup
  }
}

const mapDispathToProps = (dispatch) => {
  return {
    signUp: (creds) => dispatch(SignUp(creds))
  }
}

export default connect(mapStateToProps, mapDispathToProps)(Signup);
