import React, { Component } from 'react';
import { Chip } from 'react-materialize';

class ChatBody extends Component {
  constructor(props) {
    super(props);

    this.state = {
      message: ''
    };
  }

  componentDidUpdate({isLogged}) {
    if (isLogged && this.newData) {
      let target = this.newData;
      target.parentNode.scrollTop = target.offsetTop;
    }
  }

  handleOnChange = e => {
    this.props.onTyping(this.props.user);
    this.setState({message: e.target.value});
  }

  handleSubmit = e => {
    e.preventDefault();
    let { message } = this.state;
    let { user } = this.props;
    if (message.length <= 0) return;
    this.props.sendMessage({message, user});
    this.setState({message: ''});
  }

  render() {
    const { users, messages, msg, typing } = this.props;
    const { message } = this.state;
    const chipUsers = users.map((name) => <Chip key={name}>{name}</Chip>)
    const showMsg = messages.length > 0 ? messages.map((msg , i) => {
      return (
        <li className="chatBodyMessage" key={i} ref={(ref => this.newData = ref)}>
            <h5><b>{msg.from}</b></h5>
            <p>{msg.messages}</p>
        </li>
      );
    }) : null;
    return (
      <div className="chatWrapperBody">
          <div className="chatHeader row">
              { chipUsers }
          </div>
          <div className="chatBody row">
              { messages.length > 0 ? <div>
                  <ul>
                      { showMsg }
                  </ul>
              </div> : <h4>Start conversation</h4> }
              <form onSubmit={this.handleSubmit}>
                  <div className="chatBodyFooter input-field">
                      { (typing) ? <p className="typing"><i>{msg}</i></p> : '' }
                      <i className="material-icons prefix">mode_edit</i>
                      <input type="text" className="validate" onChange={this.handleOnChange} value={message} />
                      <button type="submit" className="waves-effect waves-light btn" disabled={!message}><i className="material-icons">send</i></button>
                  </div>
              </form>
          </div>
      </div>
    );
  }
}

export default ChatBody;
