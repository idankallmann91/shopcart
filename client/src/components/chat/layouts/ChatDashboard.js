import React, { Component } from 'react';
import Img from '../../../img.jpg';

class ChatDashboard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      userVert: '',
      search: ''
    };
  }

  openUserVertList = user => {
    (user === this.state.userVert) ?
      this.setState({userVert: ''}) :
      this.setState({userVert: user});
  }

  handleChange = e => this.setState({search: e.target.value});
  leaveChat = () => this.props.diconnectChat();

  render() {
    const { users } = this.props;
    const { userVert, search } = this.state;
    const filterUser = users.filter(user => user.toLowerCase().includes(search));
    const usersList = filterUser.map((user, i) => {
      return (
        <li className="collection-item avatar" key={i}>
           <img src={Img} alt="User" className="circle" />
           <span className="title">{user}</span>
           <p>Online</p>
           <a href="#!" onClick={() => this.openUserVertList(user)} className="secondary-content"><i className="material-icons">more_vert</i></a>
           <ul className={(user === userVert) ? "userOption z-depth-1 teal accent-4 open" : "userOption close" }>
              <li>Chat</li>
              <li>Call</li>
              <li>Video Call</li>
           </ul>
        </li>
      );
    });
    return (
      <div className="chatWrapperDashboard">
          <form autoComplete="off">
              <div className="input-field">
                  <i className="material-icons prefix">search</i>
                  <input type="search" label="search" onChange={this.handleChange} id="search" className="validate"/>
                  <label htmlFor="search">Search</label>
              </div>
          </form>
          <ul className="collection">
              { usersList }
          </ul>
          <button type="button" className="waves-effect waves-light btn" onClick={this.leaveChat}>Leave</button>
      </div>
    );
  }
}

export default ChatDashboard;
