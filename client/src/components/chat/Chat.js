import React, { Component } from 'react';
import './Chat.css';

//REDUX
import { connect } from 'react-redux';
import { ConnectChat, DiconnectChat, OnTyping, SendMessage } from '../../store/actions/socketAction';

import ChatBody from './layouts/ChatBody';
import ChatDashboard from './layouts/ChatDashboard';

class Chat extends Component {

  constructor(props) {
    super(props);

    this.state = {
      userJoin: ''
    }
  }

  joinOnChange = (e) => {
    this.setState({userJoin: e.target.value});
  }

  joinRoom = e => {
    e.preventDefault();
    let { userJoin } = this.state;
    if (!userJoin) return;
    this.props.connectChat(userJoin);
  }

  render() {
    const { userJoin } = this.state;
    const { isLogged } = this.props;
    return (
      <main className="main container">
          { isLogged ?
           <div className="chatWrapper">
              <ChatDashboard {...this.props} />
              <ChatBody {...this.props} />
          </div> :
          <div className="row">
              <h2>Join our chat</h2>
              <form onSubmit={this.joinRoom}>
                  <div className="input-field">
                      <div className="col s8">
                          <i className="material-icons prefix">account_circle</i>
                          <input type="text" onChange={this.joinOnChange} />
                          <label htmlFor="icon_prefix">Username</label>
                      </div>
                      <div className="col s4">
                          <button className="waves-effect waves-light btn btn-small" disabled={!userJoin} type="submit">Join
                              <i className="material-icons right">send</i>
                          </button>
                      </div>
                  </div>
              </form>
          </div>
        }
      </main>
    );
  }

}

const mapStateToProps = (state) => {
  // console.log(state);
  return {
    msg: state.chatReducer.msg,
    isLogged: state.chatReducer.connected,
    messages: state.chatReducer.messages,
    typing: state.chatReducer.typing,
    user: state.chatReducer.user,
    users: state.chatReducer.users
  }
}

const mapDispathToProps = (dispatch) => {
  return {
    connectChat: user => dispatch(ConnectChat(user)),
    diconnectChat: () => dispatch(DiconnectChat()),
    onTyping: user => dispatch(OnTyping(user)),
    sendMessage: msg => dispatch(SendMessage(msg))
  }
}

export default connect(mapStateToProps, mapDispathToProps)(Chat);
