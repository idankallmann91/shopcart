import React from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

//Component
import Header from './components/header/Header';
import Home from './components/home/Home';
import Result from './components/results/Results';
import Footer from './components/footer/Footer';
import Chat from './components/chat/Chat';
import Login from './components/login/Login';
import Dashboard from './components/dashboard/Dashboard';

const App = () => {
  return(
    <Router>
        <div>
            <Header />
            <Switch>
                <Route exact path="/" component={Home} />
                <Route path="/result" component={Result} />
                <Route path="/dashboard" component={Dashboard} />
                <Route path="/login" component={Login} />
                <Route path="/chat" component={Chat} />
                <Route component={Home} />
            </Switch>
            <Footer />
        </div>
    </Router>
  )
};

export default App;
