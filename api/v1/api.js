const http = require('http'),
      product = require('../../config/products.json'),
      { login, createUser, addNewProduct, getMyProducts, getResultProducts } = require('../../db/init.js');

module.exports = router => {

  router.route('/search')
    .get((req,res) => {
      const val = req.query.search;
      if (val) {
        getResultProducts(val ,products => {
          if (!products) return res.json({success: false, data: 'No Products'});
          res.json({success: true, data: products});
        })
      } else {
        res.json({success: false, data: 'Error'});
      }
    })

  router.route('/login')
    .post((req, res) => {
      const { email, password } = req.body;
      if (!email || !password) return res.json({success: false});
      login(req.body, data => {
        if(!data) return res.json({success: false});
        res.json({success: true, user: data});
      })
    })

  router.route('/signup')
    .post((req, res) => {
      const { email, password } = req.body;
      if (!email || !password) return res.json({success: false});
      createUser(req.body, data => {
        if(!data) return res.json({success: false});
        res.json({success: true});
      })
    })

  router.route('/getlist')
    .get((req, res) => {
      res.json({success: true, data: product})
    })

  router.route('/addproduct')
    .post((req, res) => {
      addNewProduct(req.body, (newProduct) => {
        if(!newProduct) return res.json({success: false});
        res.json({success: true, data: newProduct});
      })
    })

  router.route('/myproducts')
    .get((req, res) => {
      getMyProducts(products => {
        if (!products) return res.json({success: false});
        res.json({success: true, data: products});
      })
    })

  return router;

}
