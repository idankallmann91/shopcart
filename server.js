var express = require('express'),
    fs = require('fs'),
    cors = require('cors'),
    morgan = require('morgan'),
    helmet = require('helmet'),
    bodyParser = require('body-parser'),
    path = require('path'),
    io = require('./utils/socket/socket.js'),
    app = express(),
    router = express.Router(),
    port = process.env.PORT || 4000,
    db = require('./db/init.js').init(),
    appRouter = require('./api/v1/api.js')(router);

app.use(helmet());
app.use(cors());
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use('/api/v1', appRouter);

if (process.env.NODE_ENV === 'production') {

  app.use(morgan('combined'))
  app.use(express.static(path.join(__dirname, 'client', 'build')));
  app.disable('x-powered-by');

  app.get('/', (req,res) => {
    const HTML_FILE = fs.createReadStream(path.join(__dirname, 'client', 'build', 'index.html'));
    res.writeHead(200, {'Content-Type': 'text/html'});
    HTML_FILE.pipe(res);
  })

}

io(app.listen(port, () => {
  console.log(`Connected to port: ${port}`);
}));
